# Magic Square Generator

## Magic Squares

A magic square is an $`n \times n`$ matrix filled with the distinct positive 
integers in $`\{1, 2, ..., n^2\}`$ such that the sum of the integers in each row, 
column and diagonal is equal. [1][2]

Here is an example of a $`3 \times 3`$ magic square.

$`\left[\begin{matrix}
  6 & 7 & 2 \\
  1 & 5 & 9 \\
  8 & 3 & 4
\end{matrix}\right]`$

## The Software

The software defines the constraint satisfaction problem of the magic square 
and solves it, gradually outputting solutions as they are found.

### Usage

Command line invocation and arguments.
```
python msg.py -h
usage: msg.py [-h] -n N [-v] [-m] [-o O]

optional arguments:
  -h, --help  show this help message and exit
  -n N        Magic square side length.
  -v          Print variables when a solution is found.
  -m          Print matrix when a solution is found.
  -o O        Output matrix image to this directory path when a solution is
              found.

```

Sample invocation for $`3 \times 3`$ magic squares (`-n 3`) with output variables (`-v`) and image (`-o ./out`) for each solution.
```
python msg.py -n 3 -v -o ./out
{0: 8, 2: 6, 1: 1, 4: 5, 6: 4, 8: 2, 3: 3, 5: 7, 7: 9}
{0: 8, 2: 4, 1: 3, 4: 5, 6: 6, 8: 2, 3: 1, 5: 9, 7: 7}
{0: 4, 2: 8, 1: 3, 4: 5, 6: 2, 8: 6, 3: 9, 5: 1, 7: 7}
{0: 4, 2: 2, 1: 9, 4: 5, 6: 8, 8: 6, 3: 3, 5: 7, 7: 1}
{0: 2, 2: 4, 1: 9, 4: 5, 6: 6, 8: 8, 3: 7, 5: 3, 7: 1}
{0: 2, 2: 6, 1: 7, 4: 5, 6: 4, 8: 8, 3: 9, 5: 1, 7: 3}
{0: 6, 2: 2, 1: 7, 4: 5, 6: 8, 8: 4, 3: 1, 5: 9, 7: 3}
{0: 6, 2: 8, 1: 1, 4: 5, 6: 2, 8: 4, 3: 7, 5: 3, 7: 9}
```

## References

[1] https://en.wikipedia.org/wiki/Magic_square

[2] Schwartzman, Steven (1994). The Words of Mathematics: An Etymological Dictionary of Mathematical Terms Used in English. MAA. p. 130.
