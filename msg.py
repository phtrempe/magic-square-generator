import os
from argparse import ArgumentParser, ArgumentTypeError
from random import shuffle

import matplotlib.pyplot as plt
import numpy
from constraint import AllDifferentConstraint, Problem


def strictly_positive_int(v):
    v = int(v)
    if v <= 0:
        raise ArgumentTypeError(
            '{} is not a strictly positive integer.'.format(v))
    return v


def parse_args():
    parser = ArgumentParser()

    parser.add_argument('-n',
                        type=strictly_positive_int,
                        required=True,
                        help='Magic square side length.')

    parser.add_argument('-v',
                        default=False,
                        action='store_true',
                        help='Print variables when a solution is found.')

    parser.add_argument('-m',
                        default=False,
                        action='store_true',
                        help='Print matrix when a solution is found.')

    parser.add_argument('-o',
                        type=str,
                        required=False,
                        help='Output matrix image to this directory path when '
                             'a solution is found.')

    return parser.parse_args()


def shuffled(x):
    shuffle(x)
    return x


def add_row_constraints(problem, n):
    for r in range(n):
        problem.addConstraint(
            lambda *v: sum(v[:n]) == sum(v[n:]),
            (list(range(n)) + list(range(n * r, n * r + n))))


def add_column_constraints(problem, n):
    for c in range(n):
        problem.addConstraint(
            lambda *v: sum(v[:n]) == sum(v[n:]),
            (list(range(n)) + list(range(c, n**2, n))))


def add_main_diagonal_constraint(problem, n):
    problem.addConstraint(
        lambda *v: sum(v[:n]) == sum(v[n:]),
        (list(range(n)) + list((n + 1) * x for x in range(n))))


def add_secondary_diagonal_constraint(problem, n):
    problem.addConstraint(
        lambda *v: sum(v[:n]) == sum(v[n:]),
        (list(range(n)) + list((n - 1) * (x + 1) for x in range(n))))


def add_all_different_constraint(problem, n):
    problem.addConstraint(AllDifferentConstraint(), range(n**2))


def make_solution_figure(data, n, fig_w=5, fig_h=5):
    plt.figure(1, figsize=(fig_w, fig_h))
    tb = plt.table(cellText=data.astype(int),
                   loc=(0, 0), cellLoc='center')

    tc = tb.properties()['child_artists']
    for cell in tc:
        cell.set_height(1.0 / n)
        cell.set_width(1.0 / n)

    ax = plt.gca()
    ax.set_xticks([])
    ax.set_yticks([])


if __name__ == '__main__':
    args = parse_args()

    n = args.n
    out_dir = args.o
    is_print_variables = args.v
    is_print_matrix = args.m

    problem = Problem()

    for v in range(n**2):
        problem.addVariable(v, shuffled(list(range(1, n**2 + 1))))

    add_row_constraints(problem, n)
    add_column_constraints(problem, n)
    add_main_diagonal_constraint(problem, n)
    add_secondary_diagonal_constraint(problem, n)
    add_all_different_constraint(problem, n)

    if out_dir:
        os.makedirs(out_dir, exist_ok=True)

    for i, solution in enumerate(problem.getSolutionIter()):
        if is_print_variables:
            print(solution)
        if not is_print_matrix and not out_dir:
            continue
        data = numpy.array([solution[x] for x in range(n**2)]).reshape(n, n)
        if is_print_matrix:
            print(data)
        if out_dir:
            make_solution_figure(data, n)
            plt.savefig(os.path.join(out_dir, '{}x{}_{}'.format(n, n, i)))
            plt.close()
